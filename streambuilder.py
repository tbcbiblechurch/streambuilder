from kivy.app import App
from kivy.uix.widget import Widget
from kivy.graphics import Color
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.button import Button


class StreamBuilder(TabbedPanel):
    pass

class StreamBuilderApp(App):
    def build(self):
        return StreamBuilder()

if __name__ == '__main__':
    StreamBuilderApp().run()
